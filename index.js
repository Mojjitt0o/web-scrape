const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://example.com');

    // Mengambil judul dari halaman
    const title = await page.title();
    console.log('Judul halaman:', title);

    await browser.close();
})();


/*
* Buat proek baru pada VSCode
* navigasikan ke direktori proyek dan inisialisasi proyek Node.js dengan menjalankan perintah " npm init -y "
* install library Puppeteer untuk melakukan scraping dengan JavaScript. Silahkan jalankan perintah " npm install puppeteer "
* Setelah input code, silahkan jalankan sesuai nama file
*/
